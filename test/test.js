const assert = require('assert');
function compareLists(arraySample, array) {
  const listItemsCount = array.length;

  //Compare the size of the two arrays and in case of match, compare the content
  if (listItemsCount == arraySample.length) {
     for (var i = 0; i < listItemsCount; i++) {
        if (!arraySample.includes(array[i].$('span').getText())) {
          throw new Error("The element " +  i + " is not to be present in the list");
        }
     }
  } else {
     throw new Error('The number of elements of the "Awesome quotes" section does not match')
  }
}

describe('Salsita webdriver.io test sample', () => {
    before('setup', () => {
        browser.url('https://qa-homework.herokuapp.com');

        //Hit Enter Button
        $('.enterButton=Enter').click();

        //Get secret hidden in the page html
        const secret = $("input[name=secret]").getValue()

        //Enter secret into the input field
        $("input[name=code]").setValue(secret);

        //Verify the checkbox is checked if not tick it
        const checkBox = $("input[name=isRobot]");
        if (!checkBox.isSelected()) {
          checkBox.click();
        }

        //Hit Submit button
        $("button=Submit").click();
    });

    it('quotes syntax correctness', () => {
        const awesome_quotes = ["Beware of low-flying butterflies.",
          "Do something unusual today. Pay a bill.",
          "Nothing so needs reforming as other people's habits.",
          "I love deadlines. I love the whooshing sound they make as they fly by.",
          "Excellent time to become a missing person."];
        const famous_quotes = ["Yes there is a lot of people doing a great job out there.",
          "A classic is something that everyone wants to have read and nobody wants to read.",
          "You have taken yourself too seriously.",
          "It's not a bug - it's an undocumented feature.",
          "If your life was a horse, you'd have to shoot it."];

        //Load lists of quotes
        const awesome = $("[qa-id=Awesome]").$$('li');
        const famous = $("[qa-id=Famous]").$$('li');

        //Verify that the content of two provided lists matches
        compareLists(awesome_quotes, awesome);
        compareLists(famous_quotes, famous);
    });

    it('the total score evaluation status', () => {
      var totalSummary = 0;

      //Load lists of quotes
      const awesome = $("[qa-id=Awesome]").$$('li');
      const famous = $("[qa-id=Famous]").$$('li');

      //Iterate over list of quotes and add the score value to the totalSummary
      for (var i = 0; i < awesome.length; i++) {
        totalSummary += parseInt(awesome[i].$('span:nth-child(2)').getText());
      }
      for (var i = 0; i < famous.length; i++) {
        totalSummary += parseInt(famous[i].$('span:nth-child(2)').getText());
      }

      //Parse summary value from the browser
      const totalSummaryRef = parseInt($('[id=summary]').getText().replace ( /[^\d.]/g, '' ));

      //Verify that the values match
      assert.equal(totalSummary, totalSummaryRef, "The Total Summary is invalid");
    });
});
